#include <urlmon.h>
#include <tchar.h>
#include <string>

#include "../Shared/GetCurrentDirectory.h"

using namespace std;

std::string GetCurrentDirectory()
{
	char buffer[MAX_PATH] = {};
	DWORD size = GetModuleFileNameA(NULL, buffer, MAX_PATH);
	if (size == 0 || size == MAX_PATH) return "";
	std::string fileName(buffer, size);
	std::string::size_type pos = fileName.find_last_of("\\/");
	return fileName.substr(0, pos + 1);
}
