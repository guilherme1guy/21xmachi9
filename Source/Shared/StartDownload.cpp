#include <urlmon.h>
#include <tchar.h>
#include <string>

#include "StartDownload.h"
#include "GetCurrentDirectory.h"
#include "../Updater/MessageBoxHandler.h"

#pragma comment(lib, "urlmon.lib")
#pragma comment(lib,"wininet.lib")

using namespace std;

void startDownload(char* url, char* outputFile)
{
	HRESULT downloadUpdate;
	//LPBINDSTATUSCALLBACK downloadProgress;
	LPCTSTR downloadUrl = url, File = outputFile;

	string localFile = GetCurrentDirectory() + File;

	//downloadUpdate = URLDownloadToFileA(0, downloadUrl, localFile.c_str(), 0, downloadProgress);
	downloadUpdate = URLDownloadToFileA(0, downloadUrl, localFile.c_str(), 0, 0);

	switch (downloadUpdate)
	{
	case S_OK:
		updateSuccess();
		break;
	case E_OUTOFMEMORY:
		updateOOMError();
		break;
	case INET_E_DOWNLOAD_FAILURE:
		updateError();
		break;
	default:
		updateErrorUnknown();
		break;
	}
}