#pragma once

namespace UpdaterGUIProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for UpdaterGUI
	/// </summary>
	public ref class UpdaterGUI : public System::Windows::Forms::Form
	{
	public:
		UpdaterGUI(void)
		{
			InitializeComponent();
			//
			//TODO: add constructor code here.
			//
		}

	protected:
		/// <summary>
		/// Clean up used resources.
		/// </summary>
		~UpdaterGUI()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ProgressBar^ progressBar1;
	private: System::Windows::Forms::Label^ DownloadNoticeLabel;
	protected:

	private: System::Windows::Forms::Label^ label2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for designer assistance.
		/// The content of the method must not be changed with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->DownloadNoticeLabel = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// progressBar1
			// 
			this->progressBar1->Location = System::Drawing::Point(12, 30);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(472, 29);
			this->progressBar1->TabIndex = 0;
			// 
			// DownloadNoticeLabel
			// 
			this->DownloadNoticeLabel->AutoSize = true;
			this->DownloadNoticeLabel->Location = System::Drawing::Point(13, 11);
			this->DownloadNoticeLabel->Name = L"DownloadNoticeLabel";
			this->DownloadNoticeLabel->Size = System::Drawing::Size(341, 13);
			this->DownloadNoticeLabel->TabIndex = 1;
			this->DownloadNoticeLabel->Text = L"Downloading and Extracting the latest update. This will take a moment.";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(13, 64);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(101, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Downloading... 75%";
			// 
			// UpdaterGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(496, 85);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->DownloadNoticeLabel);
			this->Controls->Add(this->progressBar1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"UpdaterGUI";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Updater";
			this->TopMost = true;
			this->Load += gcnew System::EventHandler(this, &UpdaterGUI::UpdaterGUI_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void UpdaterGUI_Load(System::Object^ sender, System::EventArgs^ e) {
	}
	};
}
