#include <windows.h>
#include <winuser.h>
#include <tchar.h>
#include "MessageBoxHandler.h"

int updateError()
{
	int updateError = MessageBox(NULL, _T("The update failed to download."), _T("Error"), MB_OK | MB_ICONERROR | MB_DEFBUTTON1 | MB_SYSTEMMODAL);
	switch (updateError)
	case IDOK:
	{
		return 1;
	}
	return 0;
}

int updateOOMError()
{
	int updateOOMError = MessageBox(NULL, _T("The update failed to download due to a lack of memory."), _T("Error"), MB_OK | MB_ICONERROR | MB_DEFBUTTON1 | MB_SYSTEMMODAL);
	switch (updateOOMError)
	case IDOK:
	{
		return 1;
	}
	return 0;
}

int updateErrorUnknown()
{
	int updateErrorUnknown = MessageBox(NULL, _T("The update failed to download for some unknown reason."), _T("Error"), MB_OK | MB_ICONERROR | MB_DEFBUTTON1 | MB_SYSTEMMODAL);
	switch (updateErrorUnknown)
	case IDOK:
	{
		return 1;
	}
	return 0;
}

int updateSuccess()
{
	int updateSucceed = MessageBox(NULL, _T("The update downloaded successfully."), _T("Success"), MB_OK | MB_ICONEXCLAMATION | MB_DEFBUTTON1 | MB_SYSTEMMODAL);
	switch (updateSucceed)
	case IDOK:
	{
		return 1;
	}
	return 0;
}