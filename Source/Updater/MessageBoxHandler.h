#pragma once
#pragma comment (lib, "user32.lib")

int updateError();
int updateSuccess();
int updateOOMError();
int updateErrorUnknown();